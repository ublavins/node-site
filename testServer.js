
// code to make a web server https://www.tutorialspoint.com/nodejs/nodejs_web_module.htm

// server variables
var http = require('http');
var fs   = require('fs');
var url  = require('url');

// get info from config.json
var config = JSON.parse(fs.readFileSync("config.json"));
var host   = config.host;
var port   = config.port;

// create server
var server = http.createServer( function (request, response) {
    // parse request containing file name
    var pathname = url.parse(request.url).pathname;

    // direct user to index if pathname empty
    if (pathname === "/") {
        pathname = config.default_pathway;
    }

    console.log("Request for " + pathname + " received.");

    // read file content
    fs.readFile(pathname.substr(1), function(err, data) {
        if (err) {
            console.log(err);
            response.writeHead(404, {'Content-Type': 'text/html'});
        } else {	
            console.log("200, OK");
            response.writeHead(200, {'Content-Type': 'text/html'});	
            
            // Write the content of the file to response body
            response.write(data.toString());
        }
        response.end();
    });
});

server.listen(port, host, function() {
    console.log("Listening on " + host + ":" + port);
});

// need this for POST and GET https://www.tutorialspoint.com/nodejs/nodejs_express_framework.htm